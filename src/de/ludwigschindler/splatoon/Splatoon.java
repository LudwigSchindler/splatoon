package de.ludwigschindler.splatoon;

import de.ludwigschindler.splatoon.test.SplatoonTest;
import org.bukkit.plugin.java.JavaPlugin;

public class Splatoon extends JavaPlugin {

    //Variables
    private static JavaPlugin instance;

    public static JavaPlugin getInstance() {
        return instance;
    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    @Override
    public void onEnable() {
        instance = this;

        //registering commands
        registerCommands();

        //registering events
        registerEvents();
    }

    private void registerEvents() {
        getServer().getPluginManager().registerEvents(new SplatoonTest(), this);
    }

    private void registerCommands() {
        this.getCommand("splatoontest").setExecutor(new SplatoonTest());
    }
}
