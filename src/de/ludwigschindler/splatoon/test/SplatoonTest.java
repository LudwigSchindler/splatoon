package de.ludwigschindler.splatoon.test;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Listener;

public class SplatoonTest implements Listener, CommandExecutor {

    private static final String prefix = "[Splatoon-Test] ";
    private static boolean testMode = false;
    private static boolean debug = false;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (cmd.getName().equalsIgnoreCase("splatoontest")) {
            if (args.length == 1) {
                switch (args[0]) {
                    case "info":
                        sender.sendMessage("§7" + prefix + "Info | Test-Mode: " + (testMode ? "true" : "false"));
                        sender.sendMessage("§7" + prefix + "Info | Debugging: " + (debug ? "true" : "false"));
                        break;
                    case "items":
                    default:
                        sender.sendMessage("§c" + prefix + "Error | No valid argument specified.");
                        break;
                }
            } else if (args.length == 2) {
                String arg0 = args[0];
                String arg1 = args[1];
                if (arg0.equalsIgnoreCase("testmode") || arg0.equalsIgnoreCase("test")) {
                    if (arg1.equalsIgnoreCase("true")) {
                        testMode = true;
                        sender.sendMessage("§a" + prefix + "Success | Test-Mode set to true.");
                    } else if (arg1.equalsIgnoreCase("false")) {
                        testMode = false;
                        sender.sendMessage("§a" + prefix + "Success | Test-Mode set to false.");
                    }
                } else if (arg0.equalsIgnoreCase("debug")) {
                    if (arg1.equalsIgnoreCase("true")) {
                        debug = true;
                        sender.sendMessage("§a" + prefix + "Success | Debugging set to true.");
                    } else if (arg1.equalsIgnoreCase("false")) {
                        debug = false;
                        sender.sendMessage("§a" + prefix + "Success | Debugging set to false.");
                    }
                }
            }
        }
        return true;
    }

    private enum TestWeaponCategory {
        BLASTER,
        ROLLER,
        CHARGER
    }

    private enum TestWeapon {

        TEST_SHOOTER(TestWeaponCategory.BLASTER, 0, 0),
        TEST_ROLLER(TestWeaponCategory.ROLLER, 0, 0),
        TEST_CHARGER(TestWeaponCategory.CHARGER, 0, 0);

        private final TestWeaponCategory category;
        private final double range;
        private final double damage;

        TestWeapon(TestWeaponCategory category, double range, double damage) {
            this.category = category;
            this.range = range;
            this.damage = damage;
        }

        public TestWeaponCategory getCategory() {
            return category;
        }

        public double getRange() {
            return range;
        }

        public double getDamage() {
            return damage;
        }
    }
}
