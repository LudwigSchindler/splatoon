package de.ludwigschindler.archive;

import de.ludwigschindler.archive.objects.SplArena;
import de.ludwigschindler.archive.objects.SplPlayer;
import de.ludwigschindler.archive.objects.SplTeam;
import org.bukkit.*;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_16_R3.CraftServer;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

public class Splatoon extends JavaPlugin implements Listener {

    public static final String prefix = "§3[§bSplatoon§3] §7";
    public static Material tinte = Material.WHITE_WOOL;
    public static ArrayList<SplPlayer> inGame = new ArrayList<SplPlayer>();
    public static ArrayList<SplPlayer> inLobby = new ArrayList<SplPlayer>();
    public static HashMap<Player, Location> oldLoc = new HashMap<Player, Location>();
    public static HashMap<Player, ItemStack[]> oldItems = new HashMap<Player, ItemStack[]>();
    public static HashMap<Player, GameMode> oldGM = new HashMap<Player, GameMode>();
    public static File file = new File("plugins//Splatoon//data.yml");
    public static YamlConfiguration data = YamlConfiguration.loadConfiguration(file);
    public String hilfe = "§7--------=====§8<<< §3( §bSplatoon §3) §8>>>§7=====--------\n§7/spl create <arena> | Erstellt ein Spiel\n§7/spl delete <arena> | Löscht ein Spiel\n§7/spl join <arena> | Trete einem Spiel bei\n§7/spl leave | Verlasse ein Spiel\n§7/spl list | Listet alle Arenen auf\n§7/spl rl | Lädt die Config neu\n§7/spl info | Zeigt die Plugin Informationen\n§7/spl help | Zeigt diese Hilfeseite\n§7--------=====§8<<< §3( §bOptionen §3) §8>>>§7=====--------\n/spl option <arena> setspawn 1\n/spl option <arena> setspawn 2\n/spl option <arena> setlobby\n/spl option <arena> settime <sekunden>\n/spl option <arena> setteam 1\n/spl option <arena> setteam 2\n/spl option <arena> setDescription <Beschreibung>";

    public static void splBot(String msg) {
        Bukkit.broadcastMessage(prefix + msg);
    }

    public static void msg(SplPlayer p, String msg) {
        p.sendMessage(prefix + msg);
    }

    public static ItemStack createCustomItem(Material m, String name, String b, int i) {
        ItemStack s = new ItemStack(m, i);
        ItemMeta meta = s.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> list = new ArrayList<String>();
        list.add(b);
        meta.setLore(list);
        s.setItemMeta(meta);
        return s;
    }

    // useColorTranslation --------------------------------------------------
    public static ChatColor byteToChat(byte b) {

        ChatColor c = ChatColor.WHITE;

        if (b == 0) {
            c = ChatColor.WHITE;
        } else if (b == 1) {
            c = ChatColor.GOLD;
        } else if (b == 2) {
            c = ChatColor.DARK_PURPLE;
        } else if (b == 3) {
            c = ChatColor.AQUA;
        } else if (b == 4) {
            c = ChatColor.YELLOW;
        } else if (b == 5) {
            c = ChatColor.GREEN;
        } else if (b == 6) {
            c = ChatColor.LIGHT_PURPLE;
        } else if (b == 7) {
            c = ChatColor.DARK_GRAY;
        } else if (b == 8) {
            c = ChatColor.GRAY;
        } else if (b == 9) {
            c = ChatColor.DARK_AQUA;
        } else if (b == 10) {
            c = ChatColor.DARK_PURPLE;
        } else if (b == 11) {
            c = ChatColor.BLUE;
        } else if (b == 12) {
            c = ChatColor.DARK_RED;
        } else if (b == 13) {
            c = ChatColor.DARK_GREEN;
        } else if (b == 14) {
            c = ChatColor.RED;
        } else if (b == 15) {
            c = ChatColor.BLACK;
        }

        return c;
    }

    // useColorTranslation --------------------------------------------------
    public static String byteToName(byte b) {

        String c = "Weiß";

        if (b == 0) {
            c = "Weiß";
        } else if (b == 1) {
            c = "Orange";
        } else if (b == 2) {
            c = "Pink";
        } else if (b == 3) {
            c = "Hellblau";
        } else if (b == 4) {
            c = "Gelb";
        } else if (b == 5) {
            c = "Grün";
        } else if (b == 6) {
            c = "Rosa";
        } else if (b == 7) {
            c = "Dunkelgrau";
        } else if (b == 8) {
            c = "Grau";
        } else if (b == 9) {
            c = "Türkis";
        } else if (b == 10) {
            c = "Lila";
        } else if (b == 11) {
            c = "Blau";
        } else if (b == 12) {
            c = "Braun";
        } else if (b == 13) {
            c = "Dunkelgrün";
        } else if (b == 14) {
            c = "Rot";
        } else if (b == 15) {
            c = "Schwarz";
        }

        return c;
    }

    public static String byteToPararaph(byte b) {

        String c = "§f";

        if (b == 0) {
            c = "§f";
        } else if (b == 1) {
            c = "§6";
        } else if (b == 2) {
            c = "§5";
        } else if (b == 3) {
            c = "§b";
        } else if (b == 4) {
            c = "§e";
        } else if (b == 5) {
            c = "§a";
        } else if (b == 6) {
            c = "§d";
        } else if (b == 7) {
            c = "§8";
        } else if (b == 8) {
            c = "§7";
        } else if (b == 9) {
            c = "§3";
        } else if (b == 10) {
            c = "§5";
        } else if (b == 11) {
            c = "§1";
        } else if (b == 12) {
            c = "§6";
        } else if (b == 13) {
            c = "§2";
        } else if (b == 14) {
            c = "§c";
        } else if (b == 15) {
            c = "§0";
        }

        return c;
    }

    public static Location loadCoords(YamlConfiguration data, String path, Location loc) {

        loc.setX(data.getDouble(path + ".X"));
        loc.setY(data.getDouble(path + ".Y"));
        loc.setZ(data.getDouble(path + ".Z"));
        loc.setYaw((float) data.getDouble(path + ".Yaw"));
        loc.setPitch((float) data.getDouble(path + ".Pitch"));
        loc.setWorld(Bukkit.getWorld(data.getString(path + ".World")));

        return loc;
    }

    public static void saveCoords(File file, YamlConfiguration data, Location loc, String path) {
        data.set(path + ".X", Double.valueOf(loc.getX()));
        data.set(path + ".Y", Double.valueOf(loc.getY()));
        data.set(path + ".Z", Double.valueOf(loc.getZ()));
        data.set(path + ".Yaw", Float.valueOf(loc.getYaw()));
        data.set(path + ".Pitch", Float.valueOf(loc.getPitch()));
        data.set(path + ".World", loc.getWorld().getName());
    }

    @Override
    public void onEnable() {
        Bukkit.getPluginManager().registerEvents(this, this);

    }

    @Override
    public void onDisable() {

    }

    @EventHandler
    public void onMove(PlayerMoveEvent e) {
        SplPlayer p = new SplPlayer((CraftServer) getServer(), ((CraftPlayer) (e.getPlayer())).getHandle());

        if (p.isIngame()) {
            if (p.isSneaking()) {
                if (p.isInTinte() || p.isInTinteOnTheWall()) {

                    // Am Boden schwimmen
                    if (p.isOnGround() && p.isInTinte()) {
                        p.setVelocity(p.getLocation().getDirection().multiply(0.5).setY(p.getVelocity().getY() - 1));
                    }
                    // An der Wand
                    if (p.isInTinteOnTheWall()) {
                        p.setVelocity(p.getLocation().getDirection().multiply(0.8));
                    }
                    for (Player all : Bukkit.getOnlinePlayers()) {
                        all.spawnParticle(Particle.WATER_SPLASH, p.getLocation(), 30);
                    }
                    p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_SWIM, (float) 0.1, 1);

                }
            }
        }
    }

    @EventHandler
    public void onFoodLevel(FoodLevelChangeEvent e) {
        if (e.getEntity() instanceof Player) {
            SplPlayer p = new SplPlayer((CraftServer) getServer(), ((CraftPlayer) (e.getEntity())).getHandle());
            if (p.isinLobby() || p.isIngame()) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        SplPlayer p = new SplPlayer((CraftServer) getServer(), ((CraftPlayer) e.getPlayer()).getHandle());
        Action a = e.getAction();
        ItemStack i = e.getItem();

        if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {

            if (e.getMaterial() == Material.NAME_TAG) {
                if (i.getItemMeta().getDisplayName().equals("§bTeam wählen")) {
                    selectTeam(p, p.getArena());
                }
            } else if (e.getMaterial() == Material.CHEST) {
                if (i.getItemMeta().getDisplayName().equals("§bWaffe wählen")) {
                    // TODO Waffe wählen
                }
            } else if (e.getMaterial() == Material.NETHER_STAR) {
                if (i.getItemMeta().getDisplayName().equals("§6Erfolge")) {
                    msg(p, "§cBald verfügbar!");
                    // TODO Erfolge
                }
            } else if (e.getMaterial() == Material.SLIME_BALL) {
                if (i.getItemMeta().getDisplayName().equals("§bVerlassen")) {
                    p.performCommand("spl leave");
                }
            } else if (e.getMaterial() == Material.BLAZE_ROD) {
                if (i.getItemMeta().getDisplayName().equals("§5Lobbyphase überspringen")) {
                    p.performCommand("spl sl");
                }
            } else if (e.getMaterial() == Material.PAPER) {
                if (i.getItemMeta().getDisplayName().equals("§5Test-Scoreboard aktivieren")) {
                    p.performCommand("spl sb");
                }
            }

        }

    }

    @EventHandler
    public void onDrop(PlayerDropItemEvent e) {
        SplPlayer p = new SplPlayer((CraftServer) getServer(), ((CraftPlayer) e.getPlayer()).getHandle());

        if (p.isinLobby() || p.isIngame()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        SplPlayer p = new SplPlayer((CraftServer) getServer(),
                ((CraftPlayer) e.getWhoClicked()).getHandle());
        Inventory inv = e.getClickedInventory();
        ItemStack i = e.getCurrentItem();

        InventoryView view = e.getView();

        if (view.getTitle().startsWith("§3Team setzen...")) {
            e.setCancelled(true);
            if (i.getType() == tinte) {
                String[] split = view.getTitle().split(":");
                SplArena arena = new SplArena(split[1]);
                int team = Integer.parseInt(split[2]);
                if (team == 1) {

                    SplTeam team1 = new SplTeam((byte) i.getDurability());
                    arena.setTeam1(team1);
                    arena.save();
                } else if (team == 2) {

                    SplTeam team2 = new SplTeam((byte) i.getDurability());
                    arena.setTeam2(team2);
                    arena.save();
                }
                p.closeInventory();
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
            }
        }
        if (view.getTitle().equals("§3Team wählen...")) {
            e.setCancelled(true);
            if (i.getType() == tinte) {
                if (i.getItemMeta().getLore().get(0).contains("1")) {
                    p.setTeam(p.getArena().getTeam1());
                } else if (i.getItemMeta().getLore().get(0).contains("2")) {
                    p.setTeam(p.getArena().getTeam2());
                }
                p.closeInventory();
                p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
            }
        }

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (cmd.getName().equalsIgnoreCase("splatoon")) {
            if (sender instanceof Player) {

                SplPlayer p = new SplPlayer((CraftServer) getServer(), ((CraftPlayer) sender).getHandle());

                if (args.length == 1) {
                    if (args[0].equalsIgnoreCase("leave")) {
                        if (p.isIngame() || p.isinLobby()) {
                            p.leaveArena();
                        }
                    } else if (args[0].equalsIgnoreCase("list")) {

                        // Keine Ahnung ob ich das mache

                    } else if (args[0].equalsIgnoreCase("rl")) {

                        // Macht (noch) keinen Sinn aber egal

                    } else if (args[0].equalsIgnoreCase("info")) {

                    } else if (args[0].equalsIgnoreCase("help")) {
                        p.sendMessage(hilfe);
                    } else if (args[0].equalsIgnoreCase("sl")) {
                        p.joinArena(new SplArena("Test"));
                    } else if (args[0].equalsIgnoreCase("sb")) {
                        final SplPlayer sp = p;
                        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

                            @Override
                            public void run() {
                                setTestSb(sp);
                            }
                        }, 0, 60);

                    } else if (args[0].equalsIgnoreCase("gt")) {
                        splBot(p.getTeam().toString());
                    } else if (args[0].equalsIgnoreCase("gm")) {
                        splBot(p.getArena().getTeam1().toString());
                    }

                } else if (args.length == 2) {
                    // Arena erstellen
                    // --------------------------------------------------
                    if (args[0].equalsIgnoreCase("create")) {

                        SplArena arena = new SplArena(args[1]);

                        if (!arena.exist()) {
                            arena.create();
                            arena.save();
                            msg(p, "Du hast eine Arena mit dem Namen §b" + args[1] + " §7erstellt!");
                        } else {
                            msg(p, "§7Es existert bereits eine Arena mit dem Namen §b" + args[1] + "§7!");
                        }
                        // Arena löschen
                        // --------------------------------------------------
                    } else if (args[0].equalsIgnoreCase("delete")) {
                        SplArena arena = new SplArena(args[1]);

                        if (arena.exist()) {
                            arena.delete();
                            arena.save();
                            msg(p, "Du hast die Arena mit dem Namen §b" + args[1] + " §7gelöscht!");
                        } else {
                            msg(p, "Es existert keine Arena mit dem Namen §b" + args[1] + "§7!");
                        }
                        // Arena betreten
                        // --------------------------------------------------
                    } else if (args[0].equalsIgnoreCase("join")) {
                        SplArena arena = new SplArena(args[1]);
                        if (!p.isIngame()) {
                            if (arena.exist()) {
                                // if (arena.isReady()) {
                                // if (!arena.isFull()) {
                                p.joinLobby(arena);

                                // }
                                // } else {
                                // msg(p, "§cDie Arena ist noch nicht bereit!");
                                // }
                            } else {
                                msg(p, "§7Es existert keine Arena mit dem Namen §b" + args[1] + "§7!");
                            }
                        } else {
                            msg(p, "Du bist bereits in einem Spiel!");
                        }
                    }
                } else if (args.length == 3) {
                    if (args[0].equalsIgnoreCase("option")) {
                        if (args[2].equalsIgnoreCase("setlobby")) {
                            SplArena arena = new SplArena(args[1]);
                            if (arena.exist()) {
                                arena.setLobby(p.getLocation());
                                arena.save();
                                msg(p, "Du hast die §bLobby §7für die Arena §b" + args[1] + " §7gesetzt!");
                            } else {
                                msg(p, "§7Es existert keine Arena mit dem Namen §b" + args[1] + "§7!");
                            }
                        }
                    }
                } else if (args.length == 4) {
                    if (args[0].equalsIgnoreCase("option")) {
                        SplArena arena = new SplArena(args[1]);
                        if (arena.exist()) {
                            if (args[2].equalsIgnoreCase("setteam")) {
                                if (args[3].equalsIgnoreCase("1")) {
                                    openTeamSetGUI(p, arena, 1);
                                } else if (args[3].equalsIgnoreCase("2")) {
                                    openTeamSetGUI(p, arena, 2);
                                } else {
                                    msg(p, "Es gibt nur zwei Teams!");
                                }
                            } else if (args[2].equalsIgnoreCase("setspawn")) {
                                if (args[3].equalsIgnoreCase("1")) {
                                    arena.setSpawn1(p.getLocation());
                                    msg(p, "Du hast den §bSpawn§7 von Team §b1 §7gesetzt!");
                                } else if (args[3].equalsIgnoreCase("2")) {
                                    arena.setSpawn2(p.getLocation());
                                    msg(p, "Du hast den §bSpawn §7von Team §b2 §7gesetzt!");
                                } else {
                                    msg(p, "Es gibt nur zwei Teams!");
                                }
                            }
                            arena.save();
                        } else {
                            msg(p, "§7Es existert keine Arena mit dem Namen §b" + args[1] + "§7!");
                        }
                    }

                } else if (args.length >= 4) {

                }
            }
        }

        return true;
    }

    public void openTeamSetGUI(SplPlayer p, SplArena arena, int i) {
        Inventory inv = Bukkit.createInventory(null, 18, "§3Team setzen... §7:" + arena.getName() + ":" + i);

        for (byte j = 0; j < 16; j++) {
            inv.addItem(createCustomItem(tinte, "§bWolle", "§7Klicke zum Auswählen", 1, j));
        }

        p.openInventory(inv);
    }

    public ItemStack createCustomItem(Material m, String name, String b, int i, byte subid) {
        ItemStack s = new ItemStack(m, i, subid);
        ItemMeta meta = s.getItemMeta();
        meta.setDisplayName(name);
        ArrayList<String> list = new ArrayList<String>();
        list.add(b);
        meta.setLore(list);
        s.setItemMeta(meta);
        return s;
    }

    public ItemStack createCustomItem(Material m, String name, String b, int i, Enchantment e, int l) {
        ItemStack s = new ItemStack(m, i);
        ItemMeta meta = s.getItemMeta();
        meta.setDisplayName(name);
        meta.addEnchant(e, l, true);
        ArrayList<String> list = new ArrayList<String>();
        list.add(b);
        meta.setLore(list);
        s.setItemMeta(meta);
        return s;
    }

    public void selectTeam(SplPlayer p, SplArena arena) {
        Inventory inv = Bukkit.createInventory(null, 27, "§3Team wählen...");

        inv.setItem(11, createCustomItem(tinte, arena.getTeam1().getColoredName(), "§7Klicke um Team 1 beizutreten!", 1,
                arena.getTeam1().getByteColor()));
        inv.setItem(15, createCustomItem(tinte, arena.getTeam2().getColoredName(), "§7Klicke um Team 2 beizutreten!", 1,
                arena.getTeam2().getByteColor()));

        p.openInventory(inv);
    }
    // public void splBot(SplArena arena, String msg){
    // for (SplPlayer splPlayer : arena.getMembers()) {
    // msg(splPlayer, prefix + msg);
    // }
    // }

    public void setTestSb(SplPlayer p) {
        Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
        Objective f = board.registerNewObjective("wghh1", "wghh2");

        String ingame = "§7unbekannt";
        if (p.isIngame()) {
            ingame = "§aGame";
        } else if (p.isinLobby()) {
            ingame = "§5Lobby";
        } else if (!p.isinLobby() && !p.isIngame()) {
            ingame = "§7unbekannt";
        }

        String arena = "§7unbekannt";
        if (p.getArena() != null) {
            arena = "§6" + p.getArena().getName();
        } else {
            arena = "§7unbekannt";
        }

        // eins und zwei noch frei
        f.setDisplayName(prefix);
        f.setDisplaySlot(DisplaySlot.SIDEBAR);

        Score zwölf = f.getScore("§3Map");
        Score elf = f.getScore(arena);
        Score zehn = f.getScore("§3Team");
        Score neun = f.getScore("§ckein Team");
        if (p.getTeam() != null) {
            neun = f.getScore(p.getTeam().getColoredName());
        } else {
            neun = f.getScore("§ckein Team");
        }
        Score acht = f.getScore("§3Status");
        Score sieben = f.getScore(ingame);
        Score sechs = f.getScore("§3Waffe");
        Score fünf = f.getScore("§7unbestimmt");
        Score nul = f.getScore("§f----------------");
        Score minus = f.getScore("§bTesting...");

        zwölf.setScore(12);
        elf.setScore(11);
        zehn.setScore(10);
        neun.setScore(9);
        acht.setScore(8);
        sieben.setScore(7);
        sechs.setScore(6);
        fünf.setScore(5);
        nul.setScore(0);
        minus.setScore(-1);

        p.setScoreboard(board);
    }
}
