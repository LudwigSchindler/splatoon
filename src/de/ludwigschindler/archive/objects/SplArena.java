package de.ludwigschindler.archive.objects;

import de.ludwigschindler.archive.Splatoon;
import org.bukkit.Location;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;

import java.io.IOException;
import java.util.ArrayList;

public class SplArena {

	public static ArrayList<SplPlayer> members = new ArrayList<SplPlayer>();
	private static String name;
	private static int dauer = 300;
	private static String description = "Keine Beschreibung verfügbar!";
	private static String path;

	public SplArena(String name) {
		SplArena.name = name;
		path = "Arenen." + name;

	}

	public SplTeam getTeam1() {
		return new SplTeam((byte) Splatoon.data.getInt(path + ".teams.team1.bytecolor"));

	}

	public void setTeam1(SplTeam team) {
		team.save(Splatoon.data, path + ".teams.team1");
	}

	public SplTeam getTeam2() {
		return new SplTeam((byte) Splatoon.data.getInt(path + ".teams.team2.bytecolor"));

	}

	public void setTeam2(SplTeam team) {
		team.save(Splatoon.data, path + ".teams.team2");
	}

	public void setSpawn1(Location loc) {
		Splatoon.saveCoords(Splatoon.file, Splatoon.data, loc, path + ".locations.spawn1");
	}

	public void setSpawn2(Location loc) {
		Splatoon.saveCoords(Splatoon.file, Splatoon.data, loc, path + ".locations.spawn2");
	}

	public void setLobby(Location loc) {
		Splatoon.saveCoords(Splatoon.file, Splatoon.data, loc, path + ".locations.lobby");
	}

	public Location getSpawn1(Player p) {
		return Splatoon.loadCoords(Splatoon.data, path + ".locations.spawn1", p.getLocation());
	}

	public Location getSpawn2(Player p) {
		return Splatoon.loadCoords(Splatoon.data, path + ".locations.spawn2", p.getLocation());
	}

	public Location getLobby(Player p) {
		return Splatoon.loadCoords(Splatoon.data, path + ".locations.lobby", p.getLocation());
	}
	// public void init(){
	// getTeam1();
	// getTeam2();
	// getSpawn1();
	// getSpawn2();
	// getLobby();
	// }

	public String getName() {
		return name;
	}

	public int getDauer() {
		dauer = Splatoon.data.getInt(path + ".dauer");
		return dauer;
	}

	public void setDauer(int i) {
		dauer = i;
	}

	public void create() {
		Splatoon.data.set(path + ".name", name);
		Splatoon.data.set(path + ".dauer", 300);
	}

	public void save() {
		try {
			Splatoon.data.save(Splatoon.file);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			Splatoon.data.load(Splatoon.file);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	public void delete() {
		Splatoon.data.set("Arenen." + name, null);
	}

	public boolean exist() {
		return Splatoon.data.get(path) != null;
	}

	public String getDescription() {
		description = Splatoon.data.getString(path + ".description");
		return description;
	}

	public void setDescription(String des) {
		Splatoon.data.set(path + ".description", des);
		description = des;
	}

	public boolean isReady() {
		return true;
	}

	public ArrayList<SplPlayer> getMembers() {
		return members;
	}

	public int getNumberOfMembers() {
		return members.size();
	}

	public boolean isFull() {
		return members.size() >= 8;
	}

	public void addMember(SplPlayer p) {
		members.add(p);
	}

	public void removeMember(SplPlayer p) {
		members.remove(p);

	}

}
