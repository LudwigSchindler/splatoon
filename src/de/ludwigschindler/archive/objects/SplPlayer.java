package de.ludwigschindler.archive.objects;

import de.ludwigschindler.archive.Splatoon;
import de.ludwigschindler.archive.enums.Waffen;
import net.minecraft.server.v1_16_R3.EntityPlayer;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_16_R3.CraftServer;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;

@SuppressWarnings("deprecation")
public class SplPlayer extends CraftPlayer {

    private static SplTeam team;
    private static SplArena arena;
    private static Waffen waffe;
    private final SplPlayer p = this;

    public SplPlayer(CraftServer server, EntityPlayer p) {
        super(server, p);

    }

    public boolean isInTinte() {
        Block b = p.getLocation().subtract(0, 1, 0).getBlock();
        return b.getData() == p.getTeam().getByteColor();
    }

    public boolean isInTinteOnTheWall() {
        Location loc = p.getLocation();
        int subid = p.getTeam().getByteColor();
        Material m = Splatoon.tinte;

        loc.setX(loc.getX() - 1);
        Block a = loc.getBlock();
        loc.setX(loc.getX() + 1);
        Block b = loc.getBlock();
        loc.setZ(loc.getZ() - 1);
        Block c = loc.getBlock();
        loc.setZ(loc.getZ() + 1);
        Block d = loc.getBlock();

        return a.getType() == m && a.getData() == subid || b.getType() == m && b.getData() == subid
                || c.getType() == m && c.getData() == subid || d.getType() == m && d.getData() == subid;
    }

    public boolean isinLobby() {
        return Splatoon.inLobby.contains(p);
    }

    public boolean isIngame() {
        return Splatoon.inGame.contains(p);
    }

    public void joinLobby(SplArena arena) {
        SplPlayer.arena = arena;
        Splatoon.inLobby.add(p);
        Splatoon.oldLoc.put(p, p.getLocation());
        Splatoon.oldItems.put(p, p.getInventory().getContents());
        Splatoon.oldGM.put(p, p.getGameMode());
        p.getInventory().clear();
        p.teleport(arena.getLobby(p));
        arena.addMember(p);
        Splatoon.splBot("§b" + p.getName() + " §7hat das Spiel betreten! (§b" + arena.getNumberOfMembers() + "§7/§b8§7)");
        p.getInventory().setItem(8,
                Splatoon.createCustomItem(Material.SLIME_BALL, "§bVerlassen", "§7Drücke zum Verlassen", 1));
        p.getInventory().setItem(0, Splatoon.createCustomItem(Material.NAME_TAG, "§bTeam wählen", "§7Drücke um dein Team zu wählen", 1));
        p.getInventory().setItem(1, Splatoon.createCustomItem(Material.CHEST, "§bWaffe wählen", "§7Drücke um deine Waffe zu wählen", 1));
        p.getInventory().setItem(4, Splatoon.createCustomItem(Material.NETHER_STAR, "§6Erfolge", "§7Drücke um deine Erfolge zu sehen", 1));
        if (p.hasPermission("Splatoon.VIP")) {
            p.getInventory().setItem(3, Splatoon.createCustomItem(Material.BLAZE_ROD, "§5Lobbyphase überspringen", "§7Drücke um die Lobbyphase zu überspringen", 1));
            p.getInventory().setItem(5, Splatoon.createCustomItem(Material.PAPER, "§5Test-Scoreboard aktivieren", "§7Drücke um das Test-Scoreboard zu aktivieren", 1));
        }
        p.setGameMode(GameMode.ADVENTURE);
        p.setHealth(20);
        p.setFoodLevel(20);
        p.playSound(p.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);

    }

    public void leaveArena() {
//		p.getInventory().clear();
        arena.removeMember(p);
        Splatoon.inLobby.remove(p);
        Splatoon.inGame.remove(p);
        Splatoon.splBot("§b" + p.getName() + " §7hat das Spiel verlassen! (§b" + arena.getNumberOfMembers() + "§7/§b8§7)");
        p.setGameMode(Splatoon.oldGM.get(p));
        p.teleport(Splatoon.oldLoc.get(p));
        p.getInventory().setContents(Splatoon.oldItems.get(p));
        p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
        p.setArena(null);
        p.setTeam(null);
        p.setArena(null);
    }

    public void joinArena(SplArena arena) {
        Splatoon.inLobby.remove(p);
        Splatoon.inGame.add(p);
        p.checkTeam();
        p.teleportTeamSpawn();
        p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
    }

    public void isInAntagonisticInk() {

    }

    public SplTeam getTeam() {
        return SplPlayer.team;
    }

    public void setTeam(SplTeam team) {
        SplPlayer.team = team;
    }

    public Waffen getWaffe() {
        return waffe;
    }

    public void setWaffe(Waffen waffe) {
        SplPlayer.waffe = waffe;
    }

    public SplArena getArena() {
        return arena;

    }

    public void setArena(SplArena arena) {
        SplPlayer.arena = arena;
    }

    public void checkTeam() {
        if (p.getTeam() == null) {
            if (p.getArena().getTeam1().getNumberOfMembers() <= p.getArena().getTeam2().getNumberOfMembers()) {
                p.setTeam(p.getArena().getTeam2());
            } else {
                p.setTeam(p.getArena().getTeam1());
            }
            Splatoon.msg(p, "Du wurdest Team " + p.getTeam().getColoredName() + " §7zugewiesen");
        } else {
            Splatoon.msg(p, "Du bist in Team " + p.getTeam().getColoredName());
        }
    }

    public void teleportTeamSpawn() {
        if (p.getTeam().getByteColor() == p.getArena().getTeam1().getByteColor()) {
            p.teleport(p.getArena().getSpawn1(p));
        } else {
            p.teleport(p.getArena().getSpawn2(p));
        }

    }

}
