package de.ludwigschindler.archive.objects;

import de.ludwigschindler.archive.Splatoon;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;

public class SplTeam {

	private static final ArrayList<SplPlayer> members = new ArrayList<SplPlayer>();
	private static String name;
	private static byte byteColor;
	private static String paragraph;

	public SplTeam(byte byteColor) {
		name = Splatoon.byteToName(byteColor);
		SplTeam.byteColor = byteColor;
		paragraph = Splatoon.byteToPararaph(byteColor);
	}

	public void addMember(SplPlayer s) {
		members.add(s);
	}

	public void removeMember(SplPlayer s) {
		members.remove(s);
	}

	public boolean isMember(SplPlayer s) {
		return members.contains(s);
	}

	public String getName() {
		return name;
	}

	public String getColoredName() {
		return paragraph + name;
	}

	public byte getByteColor() {
		return byteColor;
	}

	public ArrayList<SplPlayer> getMembers() {
		return members;
	}

	public int getTinteInProzent() {
		return 0;
	}

	public int getNumberOfMembers() {
		return members.size();
	}

	public void save(YamlConfiguration data, String path) {
		data.set(path + ".name", name);
		data.set(path + ".bytecolor", byteColor);
		data.set(path + ".paragraph", paragraph);
	}

}
