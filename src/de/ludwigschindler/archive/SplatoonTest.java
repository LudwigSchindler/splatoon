package de.ludwigschindler.archive;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.BlockVector;
import org.bukkit.util.Vector;

import java.util.ArrayList;

public class SplatoonTest extends JavaPlugin implements Listener {

	int subid = 14;
	byte gegnerid = 6;
	int radius = 2;
	boolean echo = false;
	Material m = Material.PINK_CONCRETE;
	Location boje = null;
	BossBar bb = Bukkit.createBossBar("§bSpezialwaffe", BarColor.BLUE, BarStyle.SOLID);

	@Override
	public void onEnable() {
		Bukkit.getPluginManager().registerEvents(this, this);
		bb.setProgress(0D);
	}

	@Override
	public void onDisable() {

	}

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		final Player p = e.getPlayer();
		Action a = e.getAction();
		ItemStack i = e.getItem();

		if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
			if (!p.isSneaking()) {

				if (e.getMaterial() == Material.STICK && i.getItemMeta().getDisplayName().equals("§bRoller")) {
					if (p.getExp() >= 0.025) {
						klecks(p.getLocation());

						if (!bb.getPlayers().contains(p)) {
							bb.addPlayer(p);
						}
						if (bb.getProgress() <= 0.9924) {
							bb.setProgress(bb.getProgress() + 0.0005D);
						} else {
							p.getInventory().addItem(createCustomItem(Material.NETHER_STAR, "§bSpezialwaffe", "spezialiesiert...", 1));
							bb.setProgress(0);
						}

						p.setExp(p.getExp() - 0.025F);
					} else {
						p.sendMessage("§cDu hast nicht gen§gend Tinte!");
						p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
					}

				} else if (e.getMaterial() == Material.BLAZE_ROD
						&& i.getItemMeta().getDisplayName().equals("§bKleckser")) {
					if (p.getExp() >= 0.025) {

						Location loc = p.getLocation();
						loc.setY(loc.getY() + 1);
						if (!bb.getPlayers().contains(p)) {
							bb.addPlayer(p);
						}
						if (bb.getProgress() <= 0.9924) {
							bb.setProgress(bb.getProgress() + 0.0005D);
						} else {
							p.getInventory().addItem(createCustomItem(Material.NETHER_STAR, "§bSpezialwaffe", "spezialiesiert...", 1));
							bb.setProgress(0);
						}
						p.setExp(p.getExp() - 0.025F);
						Snowball sb = (Snowball) loc.getWorld().spawnEntity(loc, EntityType.SNOWBALL);
						sb.setCustomName("SPL_Kleckser");
						sb.getLocation().setDirection(p.getLocation().getDirection());
						// p.playSound(p.getLocation(),
						// Sound.ENTITY_SNOWBALL_THROW,
						// 1,
						// 1);
						sb.setVelocity(p.getLocation().getDirection().multiply(1.5));

					} else {
						p.sendMessage("§cDu hast nicht gen§gend Tinte!");
						p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
					}
				}
			}
			if (e.getMaterial() == Material.FEATHER && i.getItemMeta().getDisplayName().equals("§bSp§rbombe")) {
				if (p.getExp() >= 0.4) {
					Silverfish sf = (Silverfish) p.getLocation().getWorld().spawnEntity(p.getLocation(),
							EntityType.SILVERFISH);
					Player s = null;
					int dis = 30;
					for (Player player : p.getLocation().getWorld().getPlayers()) {

						if (p.getLocation().distance(player.getLocation()) < dis) {
							dis = (int) p.getLocation().distance(player.getLocation());
							s = player;
						}
					}
					if (dis != 30) {
						sf.setTarget(s);
						sf.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 99999, 2, false, false));
						sf.setCustomName("§6Sp§rbombe");

					} else {
						sf.remove();
					}
				} else {
					p.sendMessage("§cDu hast nicht gen§gend Tinte!");
					p.playSound(p.getLocation(), Sound.ENTITY_ITEM_BREAK, 1, 1);
				}
			} else if (e.getMaterial() == Material.GLOWSTONE_DUST
					&& i.getItemMeta().getDisplayName().equals("§bEcholokator")) {
				if (echo == false) {
					for (Player player : Bukkit.getOnlinePlayers()) {
						player.setGlowing(true);
						echo = true;
					}
				} else {
					for (Player player : Bukkit.getOnlinePlayers()) {
						player.setGlowing(false);
						echo = false;
					}
				}
			} else if (e.getMaterial() == Material.FIREWORK_ROCKET
					&& i.getItemMeta().getDisplayName().equals("§bSpringen...")) {

				Inventory inv = Bukkit.createInventory(null, 27, "§3Springen zu...");

				for (Player player : Bukkit.getOnlinePlayers()) {
					// if(!player.getName().equals(p.getName())){
					inv.addItem(
							getCustomSkull(player.getName(), "§6" + player.getName(), "§3Klicke zum teleportieren"));
					// }
				}
				p.openInventory(inv);
			}
		}
	}

	@EventHandler
	public void onFood(FoodLevelChangeEvent e) {
		e.setCancelled(true);
	}

	@EventHandler
	public void onDMG(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			// Player p = (Player) e.getEntity();
			if (e.getDamager() instanceof Silverfish && e.getDamager().getCustomName().equals("§6Sp§rbombe")) {
				for (Entity en : e.getEntity().getWorld().getEntities()) {
					if (en instanceof Silverfish && en.getCustomName().equals("§6Sp§rbome"))
						;
				}
				e.setDamage(20);
				klecks(e.getDamager().getLocation());

				e.getDamager().remove();

			} else if (e.getDamager() instanceof Snowball && e.getDamager().getCustomName().equals("SPL_Kleckser")) {
				e.setDamage(4);
			} else if (e.getDamager() instanceof Arrow && e.getDamager().getCustomName().equals("SPL_Konzentrator")) {
				e.setDamage(20);
				e.getDamager().remove();
			}
		}
		if (e.getEntity() instanceof ArmorStand) {
			e.setCancelled(true);
			e.getEntity().remove();
			boje = null;
			Bukkit.broadcastMessage(
					"§6" + e.getDamager().getName() + " §7hat eine Sprungboje von Team §3T§rkis §7zerst§rt!");
		}
	}

	@EventHandler
	public void onDmg(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			// Player p = (Player) e.getEntity();
			if (e.getCause() == DamageCause.FALL) {
				e.setCancelled(true);
			}
		}
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getClickedInventory();
		InventoryView view = e.getView();
		if (view.getTitle().equals("§3Springen zu...")) {
			Bukkit.broadcastMessage("inv (springen...) geklickt");
			e.setCancelled(true);
			if (e.getCurrentItem().getType() == Material.PLAYER_HEAD) {
				Bukkit.broadcastMessage("Kopf geklickt");
				p.teleport(Bukkit.getPlayer(e.getCurrentItem().getItemMeta().getDisplayName().substring(2)));
			}
		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {

		Player p = e.getPlayer();
		Block b = p.getLocation().subtract(0, 1, 0).getBlock();
		p.setWalkSpeed((float) 0.2);
		if (p.isSneaking()) {

			if (p.isOnGround() && b.getType() == m && b.getType() == Material.PINK_CONCRETE) {
				p.setVelocity(p.getLocation().getDirection().multiply(0.8).setY(p.getVelocity().getY() + 0.8));
				p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_SPLASH, (float) 0.3, 1);
			}
		}
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		Player p = e.getPlayer();

		if (isOtherColor(p.getLocation().subtract(0, 1, 0).getBlock())) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 30, 4, false, false));
		}
		if (isGroundColorSneak(p)) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 30, 1, false, false));
			p.setVelocity(p.getLocation().getDirection().multiply(0.5).setY(p.getVelocity().getY() - 1));
			if (p.getExp() < 1) {
				p.setExp(p.getExp() + 0.0075F);
			}
			for (Player all : Bukkit.getOnlinePlayers()) {

				all.spawnParticle(Particle.DRIP_WATER, p.getLocation(), 30);
			}

			p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_SWIM, (float) 0.1, 1);
		}
		if (umBlock(p.getLocation(), m) && p.isSneaking()) {
			p.setVelocity(p.getLocation().getDirection().multiply(0.8));
			if (p.getExp() < 1) {
				p.setExp(p.getExp() + 0.0075F);
			}
			for (Player all : Bukkit.getOnlinePlayers()) {
				all.spawnParticle(Particle.DRIP_WATER, p.getLocation(), 30);
			}
			p.playSound(p.getLocation(), Sound.ENTITY_GENERIC_SWIM, (float) 0.1, 1);
		}
	}

	@EventHandler
	public void onHit(ProjectileHitEvent e) {
		Entity en = e.getEntity();
		if (en.getCustomName().equals("SPL_Kleckser")) {
			klecks(en.getLocation());
		}
		if (en.getCustomName().equals("SPL_Konzentrator")) {
			klecks(en.getLocation());
			en.remove();

		}
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onLaunch(ProjectileLaunchEvent e) {

		if (e.getEntity() instanceof Arrow) {
			Arrow ar = (Arrow) e.getEntity();
			Player p = (Player) ar.getShooter();

			if (p.getItemInHand().getItemMeta().getDisplayName().equals("§bKonzentrator")) {
				if (!p.isSneaking()) {
					ar.setCustomName("SPL_Konzentrator");
					klecks(ar.getLocation());
				} else {
					e.setCancelled(true);
				}
			}
		}

	}

	@EventHandler
	public void oni(EntitySpawnEvent e) {
		if (e.getEntityType() == EntityType.ARMOR_STAND) {
			ArmorStand en = (ArmorStand) e.getEntity();

			en.setCustomName("§6Sprungboje");
			if (boje != null) {

				boje.getBlock().setType(Material.AIR);
				boje = en.getLocation();
			}
		}
		if (e.getEntityType() == EntityType.FIREWORK) {
			if (e.getEntity().getCustomName().equals("SPL_Sprungboje")) {
				e.setCancelled(true);
			}

		}

	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {
			Player p = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("splt")) {
				if (args.length == 1) {
					if (args[0].equalsIgnoreCase("items")) {
						// Roller
						p.getInventory().setItem(0, createCustomItem(Material.STICK, "§bRoller", "§6rollt...", 1));
						// Kleckser
						p.getInventory().setItem(1,
								createCustomItem(Material.BLAZE_ROD, "§bKleckser", "§6kleckst...", 1));
						// Konzentrator
						p.getInventory().setItem(2, createCustomItem(Material.BOW, "§bKonzentrator",
								"§6konzentriert...", 1, Enchantment.ARROW_INFINITE, 1));
						// Granate
						p.getInventory().setItem(6, createCustomItem(Material.FEATHER, "§bSp§rbombe", "§6sp§rt...", 1));
						// Echolokator
						p.getInventory().setItem(5,
								createCustomItem(Material.GLOWSTONE_DUST, "§bEcholokator", "§6echolokiert...", 1));

						// Springer
						p.getInventory().setItem(8,
								createCustomItem(Material.FIREWORK_ROCKET, "§bSpringen...", "§6springt...", 1));

						// Sprungboje
						p.getInventory().setItem(4,
								createCustomItem(Material.ARMOR_STAND, "§bSprungboje", "§6Bojanischer Sprung...", 1));
					} else if (args[0].equalsIgnoreCase("bbr")) {
						bb.removePlayer(p);
					}
				} else if (args.length == 2) {
					if (args[0].equalsIgnoreCase("color")) {
						subid = (byte) Integer.parseInt(args[1]);
					} else if (args[0].equalsIgnoreCase("radius")) {
						if (Integer.parseInt(args[1]) < 30) {
							radius = Integer.parseInt(args[1]);
						}
					} else if (args[0].equalsIgnoreCase("xp")) {
						p.setExp(Float.parseFloat(args[1]));
					}
				}
			}
		}

		return true;
	}

	@SuppressWarnings("deprecation")
	public void klecks(Location loc) {

		Vector vec = new BlockVector(loc.getBlockX(), loc.getY(), loc.getZ());

		for (int x = -radius; x <= radius; x++) {
			for (int y = -radius; y <= radius; y++) {
				for (int z = -radius; z <= radius; z++) {
					Vector position = vec.clone().add(new Vector(x, y - 1, z));

					if (vec.distance(position) <= radius + 0.5) {

						Location l = loc;
						l.setX(position.getX());
						l.setY(position.getY());
						l.setZ(position.getZ());

						Block b = l.getBlock();

						if (b.getType() != Material.AIR && b.getType() != Material.BARRIER && b.getType() != Material.STRUCTURE_BLOCK) {
							b.setType(m);
							//b.setBlockData(subid);
						}
					}
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	public boolean umBlock(Location loc, Material m) {

		loc.setX(loc.getX() - 1);
		Block a = loc.getBlock();
		loc.setX(loc.getX() + 1);
		Block b = loc.getBlock();
		loc.setZ(loc.getZ() - 1);
		Block c = loc.getBlock();
		loc.setZ(loc.getZ() + 1);
		Block d = loc.getBlock();

		return a.getType() == m && a.getType() == Material.PINK_CONCRETE || b.getType() == m && b.getType() == Material.PINK_CONCRETE
				|| c.getType() == m && c.getData() == subid || d.getType() == m && d.getData() == subid;

	}

	public ItemStack createCustomItem(Material m, String name, String b, int i) {
		ItemStack s = new ItemStack(m, i);
		ItemMeta meta = s.getItemMeta();
		meta.setDisplayName(name);
		ArrayList<String> list = new ArrayList<String>();
		list.add(b);
		meta.setLore(list);
		s.setItemMeta(meta);
		return s;
	}

	public ItemStack createCustomItem(Material m, String name, String b, int i, Enchantment e, int l) {
		ItemStack s = new ItemStack(m, i);
		ItemMeta meta = s.getItemMeta();
		meta.setDisplayName(name);
		meta.addEnchant(e, l, true);
		ArrayList<String> list = new ArrayList<String>();
		list.add(b);
		meta.setLore(list);
		s.setItemMeta(meta);
		return s;
	}

	@SuppressWarnings("deprecation")
	public boolean isGroundColorSneak(Player p) {

		Block b = p.getLocation().subtract(0, 1, 0).getBlock();

		return p.isSneaking() && p.isOnGround() && b.getType() == m && b.getType() == Material.PINK_CONCRETE;

	}

	@SuppressWarnings("deprecation")
	public boolean isOtherColor(Block b) {

		return b.getType() != Material.AIR && b.getType() == m && b.getType() != Material.PINK_CONCRETE;
	}

	public ItemStack getCustomSkull(String n, String name, String b) {
		ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
		SkullMeta metaskull = (SkullMeta) skull.getItemMeta();
		metaskull.setOwner(n);
		metaskull.setDisplayName(name);
		ArrayList<String> slist = new ArrayList<String>();
		slist.add(b);
		metaskull.setLore(slist);
		skull.setItemMeta(metaskull);

		return skull;
	}

}
