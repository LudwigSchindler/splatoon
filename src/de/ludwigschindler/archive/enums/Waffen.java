package de.ludwigschindler.archive.enums;

public enum Waffen {


	KLECKSER("Kleckser", "", Waffenkategorie.KLECKSER), AIRBRUSH_MG("Airbrush MG", "",
			Waffenkategorie.KLECKSER), DISPERSER("Disperser", "", Waffenkategorie.KLECKSER), BLASTER("Blaster", "",
			Waffenkategorie.KLECKSER), PLATSCHER("Platscher", "", Waffenkategorie.KLECKSER), KONZENTRATOR(
			"Konzentrator", "", Waffenkategorie.KONZENTRATOR), SEPIATOR("Sepiator", "",
			Waffenkategorie.KONZENTRATOR), KLECKSROLLER("Klecksroller", "",
			Waffenkategorie.ROLLER), DYNAROLLER("Dynaroller", "",
			Waffenkategorie.ROLLER), QUASTO("Quasto", "",
			Waffenkategorie.ROLLER), SCHWAPPER("Schwapper", "",
			Waffenkategorie.SCHWAPPER), SPLATLING("Splatling",
			"", Waffenkategorie.KONZENTRATOR), HYDRANT(
			"Hydrant", "",
			Waffenkategorie.SPLATLING);

	public String name;
	public String beschreibung;
	public Waffenkategorie kategorie;

	Waffen(String name, String beschreibung, Waffenkategorie kategorie) {
		this.name = name;
		this.beschreibung = beschreibung;
		this.kategorie = kategorie;
	}

}
